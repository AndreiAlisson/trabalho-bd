CFLAGS = -g
CC = gcc

all: escalona

escalona: main.c
	$(CC) $(CFLAGS) -o escalona main.c

clean:
	rm escalona
