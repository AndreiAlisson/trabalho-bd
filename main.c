#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>

enum Operacao {Write = 'W', Read = 'R', Commit = 'C'};
enum Commitado {Commitado = 1, SemCommit = 0};

/*Estrutura de dados que representa uma transação chegando (Usado pra ler a entrada do stdin)*/
typedef struct Transacoes{

	int Chegada;	/*Ordem de chegada desta transação*/
	int Tr_ID;		/*ID da transação*/
	char OP;		/*Operação sendo realizada nesta transação*/
	char Atributo;	/*Atributo no qual a operação está sendo realizada*/

}Tr;

/*Estrutura de dados que guarda os Escalonamentos presentes*/
typedef struct Escalonamentos{

	int Esc_ID;		/*ID do Escalonamento*/
	Tr *Transacoes;	/*Aponta para as transações presentes no Escalomamento atual*/
	int Index_Begin, Index_End;	/*Índices iniciais e finais dos IDs das transações*/
	int Num_Trs;	/*Quantidade de transações presentes no Escalonamento atual*/
	int Trs;

}Escalonamentos;

/*Estrutura de dados pra montar o grafo*/
struct Vertice{

	int ID;			/*ID do vértice*/
	int Vizinhos;	/*Número de vizinhos do vértice*/
	struct Vertice **border;	/*Vizinhança deste vértice*/

};

int *Pilha;
int Index;

/*Log para testes*/
void Log_Tr(Tr *T, int size){

	int i;

	for(i = 0; i < size-1; i++){
		printf("%d %d %c %c\n", T[i].Chegada, T[i].Tr_ID, T[i].OP, T[i].Atributo);
	}
	if(size > 1)
		printf("%d %d %c %c", T[i].Chegada, T[i].Tr_ID, T[i].OP, T[i].Atributo);

}

/*Log para testes*/
void Log_Esc(Escalonamentos E, int index){

	printf("Escalonamento %d(%d-%d): ", index, E.Index_Begin, E.Index_End);
	for(int i = 0; i < E.Num_Trs; i++)
		printf("%d(%c%c) ", E.Transacoes[i].Tr_ID, E.Transacoes[i].OP, E.Transacoes[i].Atributo);
	printf("\n");

}

/*Log para testes*/
void Log_Vert(struct Vertice **V, int size){

	for(int i = 0; i < size; i++){
		printf("Vértice %d: ", V[i]->ID);
		for(int j = 0; j < V[i]->Vizinhos; j++)
			printf("%d ", V[i]->border[j]->ID);
		printf("\n");
	}

}

/*Conecta um vértice A com um vértice B através da estrutura List*/
void Connect_Vertices(struct Vertice *a, struct Vertice *b) {

	if(a == NULL){
		printf("Erro Vértice a == NULL, Encerrando execução\n");
		exit(1);
	}
	if(b == NULL){
		printf("Erro Vértice b == NULL, Encerrando execução\n");
		exit(1);
	}
	for(int i = 0; i < a->Vizinhos+1; i++){
		if(a->border[i] == b){
			return;
		}
	}

	a->border = realloc(a->border, (a->Vizinhos+1) * sizeof(struct Vertice*));
	a->border[a->Vizinhos] = b;
	a->Vizinhos++;


}

bool WasVisited(int ID){

	for(int i = 0; i < Index; i++){
		if(ID == Pilha[i])
			return 1;
	}

	return 0;

}

/*Verifica se o grafo V tem ciclos, retornando 0 caso haja e 1 caso não haja*/
bool Verifica_Ciclos(struct Vertice **V, int ID){

	Pilha[Index++] = ID;

	for(int i = 0; i < V[ID]->Vizinhos; i++){

		if(WasVisited(V[ID]->border[i]->ID))
			return 0;

		if(!Verifica_Ciclos(V, V[ID]->border[i]->ID))
			return 0;

	}

	Index--;

	return 1;

}

/*Testa se um Escalonamento é serializável*/
bool Testa_Serialidade(Escalonamentos E){

	struct Vertice **V = malloc(E.Trs * sizeof(struct Vertice*));
	for(int i = 0; i < E.Trs; i++){
		V[i] = malloc(sizeof(struct Vertice));
		V[i]->border = malloc(sizeof(struct Vertice*));
		V[i]->ID = i;
		V[i]->Vizinhos = 0;
	}

	int i, j;

	for(i = 0; i < E.Num_Trs; i++)
		for(j = i+1; j < E.Num_Trs; j++){

			if(E.Transacoes[i].Tr_ID != E.Transacoes[j].Tr_ID){
				if(E.Transacoes[i].OP == Read && E.Transacoes[j].OP == Write && E.Transacoes[i].Atributo == E.Transacoes[j].Atributo){

					Connect_Vertices(V[E.Transacoes[i].Tr_ID - E.Index_Begin], V[E.Transacoes[j].Tr_ID - E.Index_Begin]);
					V[E.Transacoes[i].Tr_ID - E.Index_Begin]->ID = E.Transacoes[i].Tr_ID - E.Index_Begin;

				}
				else if(E.Transacoes[i].OP == Write && E.Transacoes[j].OP == Write && E.Transacoes[i].Atributo == E.Transacoes[j].Atributo){

					Connect_Vertices(V[E.Transacoes[i].Tr_ID - E.Index_Begin], V[E.Transacoes[j].Tr_ID - E.Index_Begin]);
					V[E.Transacoes[i].Tr_ID - E.Index_Begin]->ID = E.Transacoes[i].Tr_ID - E.Index_Begin;

				}
				else if(E.Transacoes[i].OP == Write && E.Transacoes[j].OP == Read && E.Transacoes[i].Atributo == E.Transacoes[j].Atributo){

					Connect_Vertices(V[E.Transacoes[i].Tr_ID - E.Index_Begin], V[E.Transacoes[j].Tr_ID - E.Index_Begin]);
					V[E.Transacoes[i].Tr_ID - E.Index_Begin]->ID = E.Transacoes[i].Tr_ID - E.Index_Begin;

				}
			}

		}

	Pilha = malloc(E.Num_Trs * sizeof(int));
	Index = 0;

	//Log_Vert(V, E.Trs);
	bool t = Verifica_Ciclos(V, 0);

	for(i = 0; i < E.Trs; i++){
		free(V[i]->border);
		free(V[i]);
	}
	free(V);
	free(Pilha);

	return t;

}

/*Verifica se B possui a escrita do atributo X de W na transação ID após a leitura deste X em R nesta mesma transação*/
bool Verifica_Visao(Escalonamentos A, char Xi, int IDi, char Xj, int IDj){

	for(int i = 0; i < A.Num_Trs; i++){

		if(A.Transacoes[i].OP == Read && A.Transacoes[i].Atributo == Xi && A.Transacoes[i].Tr_ID == IDi){
			for(int j = i+1; j < A.Num_Trs; j++)
				if(A.Transacoes[j].OP == Write && A.Transacoes[j].Atributo == Xj && A.Transacoes[j].Tr_ID == IDj){
					return 1;
				}
		}
	}

	return 0;

}

/*Testa a equivalencia entre dois escalonamentos*/
bool Eh_Equivalente(Escalonamentos A, Escalonamentos B){

	if(A.Transacoes[A.Num_Trs-1].OP == Write)
		if(A.Transacoes[A.Num_Trs-1].OP != B.Transacoes[A.Num_Trs-1].OP)
			if(A.Transacoes[A.Num_Trs-1].Atributo != B.Transacoes[A.Num_Trs-1].Atributo)
				if(A.Transacoes[A.Num_Trs-1].Tr_ID != B.Transacoes[A.Num_Trs-1].Tr_ID){
					return 0;
				}

	for(int i = 0; i < A.Num_Trs; i++){

		if(A.Transacoes[i].OP == Read){
			for(int j = i+1; j < A.Num_Trs; j++)
				if(A.Transacoes[j].OP == Write && A.Transacoes[j].Atributo == A.Transacoes[i].Atributo)
					if(!Verifica_Visao(B, A.Transacoes[i].Atributo, A.Transacoes[i].Tr_ID, A.Transacoes[j].Atributo, A.Transacoes[j].Tr_ID)){
						return 0;
					}
		}

	}

	return 1;

}

void troca(int vetor[], int i, int j)
{
	int aux = vetor[i];
	vetor[i] = vetor[j];
	vetor[j] = aux;
}

/*Cria combinações de escalonamentos*/
bool permuta(Escalonamentos E, int vetor[], int inf, int sup)
{

	bool t;
	Escalonamentos aux;

	if(inf == sup)
	{

		aux.Num_Trs = E.Num_Trs;
		aux.Trs = E.Trs;
		aux.Esc_ID = E.Esc_ID;
		aux.Index_Begin = E.Index_Begin;
		aux.Index_End = E.Index_End;
		aux.Transacoes = malloc(aux.Num_Trs * sizeof(Tr));

		int count = 0;
		for(int i = 0; i <= sup; i++){
			for(int j = 0; j < E.Num_Trs; j++)
				if(E.Transacoes[j].Tr_ID == vetor[i])
					aux.Transacoes[count++] = E.Transacoes[j];
		}

		if(Eh_Equivalente(E, aux)){
			free(aux.Transacoes);
			return 1;
		}
		free(aux.Transacoes);
		
	}
	else
	{
		for(int i = inf; i <= sup; i++)
		{
			troca(vetor, inf, i);
			t = permuta(E, vetor, inf + 1, sup);

			if(t)
				return 1;
			
			troca(vetor, inf, i);
		}
	}

	return 0;
}

/*Testa se um Escalonamento tem alguma equivalencia*/
bool Testa_Equivalencia(Escalonamentos E){

	int k = 0;
	Escalonamentos aux;

		int v[E.Trs];
		int count = 0;
		for(int j = E.Index_Begin; j <= E.Index_End; j++)
			v[count++] = j;

		int tam_v = E.Trs;

		if(permuta(E, v, 0, tam_v - 1))
			return 1;

	return 0;

}

/*Imprime o resultado*/
void Resultado(Escalonamentos E, int IsSer, int IsEq){

	int j, tmp_ID = 0;;

	printf("%d ", (E.Esc_ID+1));

	for(j = E.Index_Begin; j < E.Index_End; j++)
		printf("%d,", j);
	printf("%d ", j);

	if(IsSer)
		printf("SS ");
	else
		printf("NS ");

	if(IsEq)
		printf("SV\n");
	else
		printf("NV\n");

}

/*Verifica se ID da t ransação está em trans*/
bool Estah_Esc(int ID, int *trans, int trans_size){

	for(int i = 0; i < trans_size; i++)
		if(trans[i] == ID)
			return 1;

	return 0;

}

int main(int argc, char *argv[]){

	int Num_Escalonamentos, Tr_Size = 0;
	bool IsSer, IsEq;

	Tr *T;
	T = malloc(sizeof(Tr));

	do{

		T = realloc(T, (Tr_Size+1) * (sizeof(Tr)));

		scanf("%d", &T[Tr_Size].Chegada);
		scanf("%d", &T[Tr_Size].Tr_ID);
		scanf("%s", &T[Tr_Size].OP);
		scanf("%s", &T[Tr_Size].Atributo);

		Tr_Size++;

	}while(!feof(stdin));

	Escalonamentos *E;
	Num_Escalonamentos = 0;
	int tmp_ID = 0, min_ID = 10000;
	int Trs = 0, Esc_ID = 0;
	int i, j;
	int *trans = malloc(sizeof(int));
	int trans_size = 0;

	E = malloc(sizeof(Escalonamentos));
	E[Num_Escalonamentos].Transacoes = malloc(sizeof(Tr));
	E[Num_Escalonamentos].Num_Trs = 0;
	E[Num_Escalonamentos].Trs = 0;
	E[Num_Escalonamentos].Esc_ID = Esc_ID;
	Esc_ID++;

	for(i = 0; i < Tr_Size; i++){

		if(T[i].OP == Commit){

			Trs--;
			if(Trs == 0){

				Num_Escalonamentos++;
				if(i < Tr_Size-1){
					E = realloc(E, (Num_Escalonamentos+1) * sizeof(Escalonamentos));
					E[Num_Escalonamentos].Transacoes = malloc(sizeof(Tr));
					E[Num_Escalonamentos].Num_Trs = 0;
					E[Num_Escalonamentos].Esc_ID = Esc_ID;
					Esc_ID++;
				}

				tmp_ID = 0;
				min_ID = 1000;

				for(int i = 0; i < trans_size; i++){
					if(trans[i] > tmp_ID)
						tmp_ID = trans[i];
					if(trans[i] < min_ID)
						min_ID = trans[i];
				}

				E[Num_Escalonamentos-1].Index_Begin = min_ID;
				E[Num_Escalonamentos-1].Index_End = tmp_ID;
				tmp_ID = min_ID = 0;
				E[Num_Escalonamentos-1].Trs = E[Num_Escalonamentos-1].Index_End - E[Num_Escalonamentos-1].Index_Begin + 1;

				free(trans);
				trans = malloc(sizeof(int));
				trans_size = 0;

			}

		}
		else if(!Estah_Esc(T[i].Tr_ID, trans, trans_size)){/****************************/

			trans = realloc(trans, trans_size+1 * sizeof(int));
			trans[trans_size++] = T[i].Tr_ID;

			Trs++;
			E[Num_Escalonamentos].Num_Trs++;
			E[Num_Escalonamentos].Transacoes = realloc(E[Num_Escalonamentos].Transacoes, E[Num_Escalonamentos].Num_Trs * sizeof(Tr));
			E[Num_Escalonamentos].Transacoes[E[Num_Escalonamentos].Num_Trs-1] = T[i];
		}
		else{

			E[Num_Escalonamentos].Num_Trs++;
			E[Num_Escalonamentos].Transacoes = realloc(E[Num_Escalonamentos].Transacoes, E[Num_Escalonamentos].Num_Trs * sizeof(Tr));
			E[Num_Escalonamentos].Transacoes[E[Num_Escalonamentos].Num_Trs-1] = T[i];

		}

	}

	/*Para cada Escalonamento testa sua serialidade e equivalencia*/
	for(int i = 0; i < Num_Escalonamentos; i++){
		IsSer = Testa_Serialidade(E[i]);

		IsEq = Testa_Equivalencia(E[i]);

		Resultado(E[i], IsSer, IsEq);
	}

	free(T);
	for(int c = 0; c < Num_Escalonamentos; c++)
		free(E[c].Transacoes);
	free(E);
	free(trans);

	return 0;
}