#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

/*Estrutura de dados pra associar um vértice ao outro*/

typedef struct tr{

	int IDTr;
	char operacao;
	char atributo;


}Tr;

typedef struct atributo{

	char nome;
	int numRead;
	int read[5];
	int numWrite;
	int write[5];
	
}Atributo;

/*Estrutura de dados pra montar o grafo*/
typedef struct vertice{

	bool WasVisited;	/*Utilizado no algoritmo de djikstra para verificação de ciclo*/
	int nivel;
	struct vertice *border;	/*Vizinhança deste vértice*/
	int numVizinhos;
	int ID;

}Vert;

/*Conecta um vértice A com um vértice B através da estrutura List*/
void Connect_Vertices(Vert *g, int a, int b){


	g[a].border[g[a].numVizinhos]=g[b];
	g[a].numVizinhos++;

}

bool buscaEmProfundidade(Vert *V, int nivel){

	int i;
	bool ciclo;	

	V->WasVisited=true;
	V->nivel = nivel;
	ciclo = false;
	
	for(i=0;i<V->numVizinhos && ciclo == false;i++){
		if(V->border[i].WasVisited==0)
			ciclo = buscaEmProfundidade(&V->border[i],nivel+1);

		else if(V->nivel > V->border[i].nivel)
			return true;
			
	}

	return ciclo;



}

/*Verifica se o grafo V tem ciclos, retornando 0 caso haja e 1 caso não haja*/
bool Verifica_Ciclos(Vert *V, int inicio, int Num_Trs){

	int i,j;
	bool ciclo=false;
	
	for(int i = inicio; i < Num_Trs+inicio && ciclo==false; i++){
		for(int j = inicio; j < Num_Trs+inicio; j++)
			V[j].WasVisited=false;

		ciclo = buscaEmProfundidade(&V[i], 0);

		
	}

	return ciclo;
	

}

void main(){

	char entrada[50],op,atributo;
	int i =0,j,numCommit=0, IDTr,numTr,numAtributos,novo=0, numTransacoes=0,inicio=0, numEscalonamento=0;

	bool ciclo;

	Tr tr[50];
	Atributo a[50];

	Vert *V = malloc(10 * sizeof(Vert));

	for(i = 0; i < 10; i++){
		V[i].numVizinhos = 0;
		V[i].WasVisited = 0;
		V[i].ID = i+1;
		V[i].border = malloc(10 * sizeof(Vert));
	}

	
	
	while(scanf("%s", entrada)!=EOF){

		scanf("%s", entrada);
		IDTr = 	atoi(entrada);
		//printf("ID: %d\n", IDTr);
		scanf("%s", entrada);
		op = entrada[0];
		//printf("op: %c\n", op);
		scanf("%s", entrada);
		atributo = entrada[0];
		//printf("atributo: %c\n", atributo);
	
		if(IDTr > novo){
			numCommit++;
			novo++;
		}
		
		
		if(op!='C'){	 
			i=0;
			while(a[i].nome!=atributo && i<numAtributos)
				i++;

			if(i==numAtributos){
				a[i].nome = atributo;
				a[i].numRead = 0;
				a[i].numWrite = 0;
				numAtributos++;
			}	


			if(op=='R'){
				a[i].read[a[i].numRead]=IDTr;
				a[i].numRead++;

				for(j=0;j<a[i].numWrite;j++)
					if(a[i].write[j]!=IDTr)				
						Connect_Vertices(V, a[i].write[j]-1, IDTr-1);
			}
			else{
				a[i].write[a[i].numWrite]=IDTr;
				a[i].numWrite++;
		
				for(j=0;j<a[i].numRead;j++)
					if(a[i].read[j]!=IDTr)				
						Connect_Vertices(V, a[i].read[j]-1, IDTr-1);

				for(j=0;j<a[i].numWrite;j++)
					if(a[i].write[j]!=IDTr)				
						Connect_Vertices(V, a[i].write[j]-1, IDTr-1);

			}

		}

		else{
			numCommit--;
			numTransacoes++;
		
		}
			
	
		tr[numTr].IDTr = IDTr;
		tr[numTr].operacao = op;
		tr[numTr].atributo = atributo;
		
		numTr++;

		if(numCommit==0){

			
			ciclo = Verifica_Ciclos(V, inicio, numTransacoes);
			printf("%d ", numEscalonamento+1);
	
	
			for(i = inicio; i < numTransacoes+inicio-1; i++)
				printf("%d,", i+1);
			printf("%d ", numTransacoes+inicio);

			if(ciclo)
				printf("NS ");
			else	printf("SS ");

			printf("\n");

			/*for(i = inicio; i < numTransacoes+inicio; i++){
				printf("%d: ", i+1);
				for(j=0;j<V[i].numVizinhos;j++)
					printf("%d ", V[i].border[j].ID);
				printf("\n");
			}*/

			inicio+=numTransacoes;

						

			numAtributos=0;
			numTr=0;
			numTransacoes=0;
			numEscalonamento++;
	
			for(i = 0; i < 10; i++){
				V[i].numVizinhos = 0;
				V[i].WasVisited = 0;
			}
	
		

	
		}

	}
	

}











